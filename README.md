![build status](https://gitlab.com/balibou/word-counter-util/badges/master/build.svg)
![coverage report](https://gitlab.com/balibou/word-counter-util/badges/master/coverage.svg)<br />
[![npm package](https://img.shields.io/npm/v/word-counter-util/latest.svg)](https://www.npmjs.com/package/word-counter-util)

# word-counter-util

This repo is a library which usefully tells us the count of same words in a given string

```js
console.log(wordCounterUtil('hello world hello'));
// ['hello appears 2 time(s)', 'world appears 1 time(s)']
```

## Getting started

### CommonJS and ES environment
* `npm install word-counter-util` or `yarn add word-counter-util`
* For CommonJS: `require('word-counter-util')`
* For ES: `import wordCounterUtil from 'word-counter-util'`

### For any environment (example with browser):
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>wordCounterUtil demo</title>
  </head>
  <body>
    <script src="http://benjamin.cherion.free.fr/word-counter-util.umd.js"></script>
    <script>console.log(wordCounterUtil('world hello wor/ld hello d'))</script>
  </body>
</html>
```
## License

[MIT](LICENSE).