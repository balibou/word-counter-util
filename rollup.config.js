import babel from 'rollup-plugin-babel';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';

const input = 'src/main.js';

function isProductionNodeEnv() {
  return process.env.NODE_ENV === 'production' ? terser() : null;
}

export default [{
  input,
  output: [
    { file: pkg.main, format: 'cjs' },
    { file: pkg.browser, name: 'wordCounterUtil', format: 'umd' },
  ],
  plugins: [
    babel(),
    isProductionNodeEnv(),
  ],
},
{
  input,
  output: [
    { file: pkg.module, format: 'es' },
  ],
  plugins: [
    isProductionNodeEnv(),
  ],
}];
