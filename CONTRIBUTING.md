## Getting started

Clone this repository and install its dependencies:

```bash
git clone https://gitlab.com/balibou/word-counter-util.git
cd word-counter-util
yarn
yarn dev
```

`yarn run build` builds the library to `dist`, generating three files:

* `dist/word-counter-util.cjs.js`
    A CommonJS bundle, suitable for use in Node.js, that `require`s the external dependency. This corresponds to the `"main"` field in package.json
* `dist/word-counter-util.esm.js`
    an ES module bundle, suitable for use in other people's libraries and applications, that `import`s the external dependency. This corresponds to the `"module"` field in package.json
* `dist/word-counter-util.umd.js`
    a UMD build, suitable for use in any environment (including the browser, as a `<script>` tag), that includes the external dependency. This corresponds to the `"browser"` field in package.json

`yarn run dev` builds the library, then keeps rebuilding it whenever the source files change using [rollup-watch](https://github.com/rollup/rollup-watch).

`yarn run test` tests the library.