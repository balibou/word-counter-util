import { isString, isEmptyString } from './utils/string';
import mapWordCounter from './utils/mapWordCounter';

/**
 * Return the count of same words in input
 * @param {string} input
 * @return {array}
 */

export default function wordCounterUtil(input) {
  try {
    if (!isString(input)) {
      throw new Error('Argument is not a string');
    }

    if (isEmptyString(input)) {
      throw new Error('String is empty');
    }

    const hashTableWordCounter = mapWordCounter(input);

    if (Object.keys(hashTableWordCounter).length === 0) {
      throw new Error('There is no valid word to treat');
    }

    return Object
      .keys(hashTableWordCounter)
      .map(word => `${word} appears ${hashTableWordCounter[word]} time(s)`);
  } catch (error) {
    return error;
  }
}
