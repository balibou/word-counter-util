import wordCounter from './main';

describe('main', () => {
  describe('wordCounter', () => {
    test('returns error if argument is not a string', () => {
      expect(wordCounter([])).toEqual(new Error('Argument is not a string'));
    });

    test('returns error if argument is an empty string', () => {
      expect(wordCounter('')).toEqual(new Error('String is empty'));
    });

    test('returns error if there is no valid word to treat', () => {
      expect(wordCounter('tes/t')).toEqual(new Error('There is no valid word to treat'));
    });

    test('returns list with count of same words in a string', () => {
      expect(wordCounter('hello test hello')).toEqual(['hello appears 2 time(s)', 'test appears 1 time(s)']);
    });
  });
});
