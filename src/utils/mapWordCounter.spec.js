import mapWordCounter from './mapWordCounter';

describe('mapWordCounter', () => {
  test('should map right count of same word in a string', () => {
    const obj = { test: 1, hello: 2 };

    expect(mapWordCounter('hello test hello')).toEqual(obj);
  });
});
