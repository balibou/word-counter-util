import { isAlphabeticalString, isString, isEmptyString } from './string';

describe('string', () => {
  test('isAlphabeticalString returns true if a string has only alphabetical characters', () => {
    expect(isAlphabeticalString('test')).toBeTruthy();
    expect(isAlphabeticalString('t/est')).toBeFalsy();
  });

  test('isString returns true if an input is type of string', () => {
    expect(isString('test')).toBeTruthy();
    expect(isString([])).toBeFalsy();
  });

  test('isEmptyString return true if a string is empty', () => {
    expect(isEmptyString('')).toBeTruthy();
    expect(isEmptyString('test')).toBeFalsy();
  });
});
