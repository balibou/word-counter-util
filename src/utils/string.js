/**
 * Returns if a string is composed with only letters
 * @param {string} str
 * @return {boolean}
 */

function isAlphabeticalString(str) {
  const letters = /^[A-Za-z]+$/;

  return str.match(letters);
}

/**
 * Returns if an input is a string
 * @param input
 * @return {boolean}
 */

function isString(input) {
  return typeof input === 'string';
}

/**
 * Returns if a string is empty
 * @param {string} str
 * @return {boolean}
 */

function isEmptyString(str) {
  return str.length === 0;
}

export { isAlphabeticalString, isString, isEmptyString };
