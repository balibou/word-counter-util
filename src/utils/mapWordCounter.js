import { isAlphabeticalString } from './string';

/**
 * Return a hashtable with count of each word in a string
 * @param {string} str
 * @return {object}
 */

export default function mapWordCounter(str) {
  const hashTable = {};
  const wordList = str.split(' ');
  const wordListLength = wordList.length;

  for (let i = 0; i < wordListLength; i += 1) {
    if (isAlphabeticalString(wordList[i]) && wordList[i].length > 1) {
      if (hashTable[wordList[i]] === undefined) hashTable[wordList[i]] = 0;
      hashTable[wordList[i]] += 1;
    }
  }

  return hashTable;
}
